package xyz.artrinix.milo.common.database.types

enum class LevelAnnouncementChannelType {
    SAME_CHANNEL, DIRECT_MESSAGE, DIFFERENT_CHANNEL
}