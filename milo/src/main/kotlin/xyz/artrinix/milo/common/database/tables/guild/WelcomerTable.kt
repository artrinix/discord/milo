package xyz.artrinix.milo.common.database.tables.guild

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import xyz.artrinix.milo.common.database.types.WelcomerType

object WelcomerTable : LongIdTable("welcomer") {
    // On join
    val sendOnJoin = bool("send_on_join").default(false)
    val joinChannelId = long("channel_join").nullable()
    val joinMessage = text("join_message").nullable()
    val joinType = enumeration("join_type", WelcomerType::class).default(WelcomerType.EMBED)
    val deleteJoinMessagesAfter = long("delete_join_messages_after").nullable()
    // On leave
    val sendOnLeave = bool("send_on_leave").default(false)
    val leaveChannelId = long("channel_leave").nullable()
    val leaveMessage = text("leave_message").nullable()
    val leaveType = enumeration("leave_type", WelcomerType::class).default(WelcomerType.EMBED)
    val deleteLeaveMessagesAfter = long("delete_leave_messages_after").nullable()
    // Send welcome to DM
    val sendWelcomeToDM = bool("send_welcome_to_dm").default(false)
    val welcomeMessage = text("welcome_message").nullable()
    // DM on ban
    val sendMessageOnBan = bool("send_message_on_ban").default(false)
    val bannedMessage = text("banned_message").nullable()
}

class Welcomer(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Welcomer>(WelcomerTable)

    // On join
    var sendOnJoin by WelcomerTable.sendOnJoin
    var joinChannelId by WelcomerTable.joinChannelId
    var joinMessage by WelcomerTable.joinMessage
    var joinType by WelcomerTable.joinType
    var deleteJoinMessagesAfter by WelcomerTable.deleteJoinMessagesAfter
    // On leave
    var sendOnLeave by WelcomerTable.sendOnLeave
    var leaveChannelId by WelcomerTable.leaveChannelId
    var leaveMessage by WelcomerTable.leaveMessage
    var leaveType by WelcomerTable.leaveType
    var deleteLeaveMessagesAfter by WelcomerTable.deleteLeaveMessagesAfter
    // Send welcome to DM
    var sendWelcomeToDM by WelcomerTable.sendWelcomeToDM
    var welcomeMessage by WelcomerTable.welcomeMessage
    // DM on ban
    var sendMessageOnBan by WelcomerTable.sendMessageOnBan
    var bannedMessage by WelcomerTable.bannedMessage
}