package xyz.artrinix.milo.common.database

import mu.KotlinLogging
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.artrinix.milo.common.database.tables.guild.GuildTable
import xyz.artrinix.milo.common.database.tables.guild.GuildUserTable
import xyz.artrinix.milo.common.database.tables.guild.WelcomerTable
import xyz.artrinix.milo.common.database.tables.guild.autoRole.AutoRoleConfig
import xyz.artrinix.milo.common.database.tables.guild.leveling.ExperienceRoleTable
import xyz.artrinix.milo.common.database.tables.guild.leveling.LevelingAnnouncementTable
import xyz.artrinix.milo.common.database.tables.guild.leveling.LevelingTable
import xyz.artrinix.milo.common.database.tables.guild.leveling.LevelingTableToRole
import xyz.artrinix.milo.utils.config.ConfigFramework.Companion.config
import kotlin.jvm.Throws

object DatabaseFramework {
    private val logger = KotlinLogging.logger {  }

    /**
     * An instance of the one and only connection to the database.
     * @returns Database
     */
    lateinit var db: Database

    private val host: String = config.database.postgres.host
    private val port: Int = config.database.postgres.port
    private val databaseName: String = config.database.postgres.database
    private val user: String = config.database.postgres.user
    private val password: String = config.database.postgres.password

    init {
        logger.debug { "Initialized"}
    }

    @Throws(Exception::class)
    fun connect() {
        try {
            logger.info { "Connecting to pgsql://$user@$host:$port/$databaseName" }
            db = Database.connect(
                url = "jdbc:postgresql://$host:$port/$databaseName",
                driver = "org.postgresql.Driver",
                user = user,
                password = password
            )

            transaction {
                addLogger(Slf4jSqlDebugLogger)

                if (!connection.isClosed) {
                    logger.info { "Connected to the database." }
                } else {
                    try {
                        connect()
                        logger.warn { "This shouldn't happen? the connection to the pgsql database is closed but didn't throw an exception?" }
                    } catch (err: Error) {
                        throw err
                    }
                }
            }

            registerSchema()
        } catch (err: Error) {
            throw err
        }
    }

    /**
     * Checks if the database is still connected
     * @returns Boolean
     */
    fun isConnected(): Boolean = transaction {
        try {
            !connection.isClosed
        } catch (e: Exception) {
            false
        }
    }

    private fun registerSchema() {
        transaction {
            addLogger(Slf4jSqlDebugLogger)
            // Create missing Tables
            SchemaUtils.createMissingTablesAndColumns(
                AutoRoleConfig,
                ExperienceRoleTable,
                LevelingTableToRole,
                LevelingAnnouncementTable,
                LevelingTable,
                GuildTable,
                GuildUserTable,
                WelcomerTable
            )
        }
    }
}