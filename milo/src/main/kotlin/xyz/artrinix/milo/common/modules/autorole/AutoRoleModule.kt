package xyz.artrinix.milo.common.modules.autorole

import dev.minn.jda.ktx.await
import dev.minn.jda.ktx.listener
import mu.KotlinLogging
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.api.sharding.ShardManager
import xyz.artrinix.milo.common.database.tables.guild.autoRole.AutoRole
import xyz.artrinix.milo.utils.newSuspendedTransaction

class AutoRoleModule(shardManager: ShardManager) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    init {
        shardManager.listener<GuildMemberJoinEvent> { event ->
            val autoRole = newSuspendedTransaction {
                AutoRole.findById(event.guild.idLong)
            } ?: return@listener

            if (!autoRole.enabled) return@listener

            event.guild.modifyMemberRoles(event.member, autoRole.roles.map { event.guild.getRoleById(it) }, listOf()).await()
        }
    }
}