package xyz.artrinix.milo.common.database.tables.guild.leveling

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import xyz.artrinix.milo.common.database.types.LevelAnnouncementChannelType
import xyz.artrinix.milo.common.database.types.MessageType

object LevelingAnnouncementTable : LongIdTable("leveling_announcement") {
    val type = enumeration("level_up_message_type", MessageType::class).default(MessageType.EMBED)
    val channelToSend = enumeration("level_up_channel", LevelAnnouncementChannelType::class).default(
        LevelAnnouncementChannelType.SAME_CHANNEL)
    val channelId = long("channel_id").nullable()
    val onlyIfUserReceivedRoles = bool("only_if_user_received_roles").default(false)
    val message = text("level_up_message").nullable()
}

class LevelingAnnouncement(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<LevelingAnnouncement>(LevelingAnnouncementTable)

    var type by LevelingAnnouncementTable.type
    var channelToSend by LevelingAnnouncementTable.channelToSend
    var channelId by LevelingAnnouncementTable.channelId
    var onlyIfUserReceivedRoles by LevelingAnnouncementTable.onlyIfUserReceivedRoles
    var message by LevelingAnnouncementTable.message
}