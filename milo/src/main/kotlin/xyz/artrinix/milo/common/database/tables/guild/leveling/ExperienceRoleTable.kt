package xyz.artrinix.milo.common.database.tables.guild.leveling

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.LongColumnType
import xyz.artrinix.milo.common.database.exposed.array

object ExperienceRoleTable : IntIdTable("experience_role") {
    val guildId = long("guild_id")
    val roles = array<Long>("role_id", LongColumnType()).default(arrayOf())
    val rate = double("rate").default(0.0)
    val requiredLevel = long("required_level")
}

class ExperienceRole(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<ExperienceRole>(ExperienceRoleTable)

    var guildId by ExperienceRoleTable.guildId
    var roles by ExperienceRoleTable.roles
    var rate by ExperienceRoleTable.rate
    var requiredLevel by ExperienceRoleTable.requiredLevel
}