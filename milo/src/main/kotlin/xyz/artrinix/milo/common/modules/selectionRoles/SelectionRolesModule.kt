package xyz.artrinix.milo.common.modules.selectionRoles

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.await
import dev.minn.jda.ktx.interactions.SelectOption
import dev.minn.jda.ktx.interactions.SelectionMenu
import dev.minn.jda.ktx.listener
import net.dv8tion.jda.api.entities.Emoji
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.events.interaction.SelectionMenuEvent
import net.dv8tion.jda.api.sharding.ShardManager

// @TODO Migrate this to use the Database
class SelectionRolesModule(shardManager: ShardManager) {
    companion object {
        val pronouns = listOf(
            SelectOption(
                "He/Him",
                "857688416781992026",
                "Masculine pronouns",
                Emoji.fromUnicode("♂️")
            ),
            SelectOption(
                "She/Her",
                "857688189389111337",
                "Feminine pronouns",
                Emoji.fromUnicode("♀️")
            ),
            SelectOption(
                "They/Them",
                "857688598446997514",
                "Neutral pronouns",
                Emoji.fromUnicode("⚧")
            ),
            SelectOption(
                "Any",
                "905707797054894083",
                "You're fine with anything",
                Emoji.fromUnicode("♾️")
            ),
            SelectOption(
                "Ask",
                "881129027040084019",
                "Request to be asked your pronouns",
                Emoji.fromUnicode("*️⃣")
            ),
            SelectOption(
                "Name only",
                "879639203988779008",
                "Refer to me by only my name or with mentions",
                Emoji.fromUnicode("⛔")
            )
        )

        val optional = listOf(
            SelectOption(
                "Artrinix SMP",
                "857547644058337280",
                "Gain access to the modded SMP server"
            ),
            SelectOption(
                "Question Of The Day",
                "783948985031065620",
                "Grant yourself the role to be mentioned for the QOTD"
            ),
            SelectOption(
                "Beta News",
                "927796753166663710",
                "Be mentioned for when we do public beta tests or release beta updates"
            ),
            SelectOption(
                "PC",
                "905707800200634378",
                "You play games on a PC"
            ),
            SelectOption(
                "PlayStation",
                "905707806018117632",
                "You play games on a PlayStation"
            ),
            SelectOption(
                "Xbox",
                "905707806752120853",
                "You play games on a Xbox"
            ),
            SelectOption(
                "Switch",
                "905707807372873748",
                "You play games on a Switch"
            ),
            SelectOption(
                "Mobile",
                "907971168546471987",
                "You play games on a Mobile"
            )
        )

        fun setupMenu(channel: TextChannel) {
            channel
                .sendMessage("> Select your preferred pronouns")
                .setActionRow(SelectionMenu(
                    "selection_roles:${channel.guild.id}:${channel.id}:pronouns",
                "None selected",
                    1 .. pronouns.size,
                    false,
                    pronouns
                )).queue()

            channel
                .sendMessage("> Select your optional roles")
                .setActionRow(SelectionMenu(
                    "selection_roles:${channel.guild.id}:${channel.id}:optional",
                    "None selected",
                    1 .. optional.size,
                    false,
                    optional
                )).queue()
        }
    }
    init {
        shardManager.listener<SelectionMenuEvent> { event ->
            val msg = event.deferReply(true).await()
            val roles = event.selectedOptions?.map { event.guild!!.getRoleById(it.value)!! }!!

            when (event.interaction.selectionMenu!!.id) {
                "selection_roles:587965413682446336:711753509535744112:pronouns" -> {
                    val allRoles = pronouns.map { event.guild!!.getRoleById(it.value)!! }

                    event.guild!!.modifyMemberRoles(event.member!!, roles, allRoles.filter { !roles.contains(it) }).await()
                }

                "selection_roles:587965413682446336:711753509535744112:optional" -> {
                    val allRoles = optional.map { event.guild!!.getRoleById(it.value)!! }

                    event.guild!!.modifyMemberRoles(event.member!!, roles, allRoles.filter { !roles.contains(it) }).await()
                }

                else -> return@listener
            }

            msg.editOriginalEmbeds(Embed {
                description = "> Your roles have been updated!"
            }).await()
        }
    }
}