package xyz.artrinix.milo.common.database.types

enum class RoleGiveType {
    STACK, REMOVE
}