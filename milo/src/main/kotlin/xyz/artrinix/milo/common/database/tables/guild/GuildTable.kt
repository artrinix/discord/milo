package xyz.artrinix.milo.common.database.tables.guild

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.LongColumnType
import org.jetbrains.exposed.sql.ReferenceOption
import org.jetbrains.exposed.sql.TextColumnType
import xyz.artrinix.milo.common.database.exposed.array
import xyz.artrinix.milo.common.database.tables.guild.autoRole.AutoRole
import xyz.artrinix.milo.common.database.tables.guild.autoRole.AutoRoleConfig
import xyz.artrinix.milo.common.database.tables.guild.leveling.Leveling
import xyz.artrinix.milo.common.database.tables.guild.leveling.LevelingTable

object GuildTable : LongIdTable("guild") {
    // Guild settings
    val commandPrefix = text("prefix").default(";")
    val disabledCommands = array<String>("disabled_commands", TextColumnType()).default(arrayOf())
    val blacklistedChannels = array<Long>("blacklisted_commands", LongColumnType()).default(arrayOf())
    // Modules
    val autoRole = reference("auto_role_id", AutoRoleConfig, onDelete = ReferenceOption.CASCADE)
    val leveling = reference("leveling_id", LevelingTable, onDelete = ReferenceOption.CASCADE)
    val welcomer = reference("welcomer_id", WelcomerTable, onDelete = ReferenceOption.CASCADE)
}

class Guild(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Guild>(GuildTable)

    var commandPrefix by GuildTable.commandPrefix
    var disabledCommands by GuildTable.disabledCommands
    var blacklistedChannels by GuildTable.blacklistedChannels

    var autoRole by AutoRole referencedOn GuildTable.autoRole
    var leveling by Leveling referencedOn GuildTable.leveling
    var welcomer by Welcomer referencedOn GuildTable.welcomer
}