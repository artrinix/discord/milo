package xyz.artrinix.milo.common.database.types

enum class WelcomerType {
    TEXT, EMBED, IMAGE
}