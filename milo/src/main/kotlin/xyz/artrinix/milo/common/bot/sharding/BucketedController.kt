package xyz.artrinix.milo.common.bot.sharding

import net.dv8tion.jda.api.utils.SessionController
import net.dv8tion.jda.api.utils.SessionControllerAdapter
import javax.annotation.CheckReturnValue
import javax.annotation.Nonnegative
import javax.annotation.Nonnull

class BucketedController(@Nonnegative bucketFactor: Int, homeGuildId: Long) : SessionControllerAdapter() {
    private val shardControllers: Array<SessionController?>

    constructor(homeGuildId: Long) : this(16, homeGuildId)

    override fun appendSession(@Nonnull node: SessionController.SessionConnectNode) {
        controllerFor(node)!!.appendSession(node)
    }

    override fun removeSession(@Nonnull node: SessionController.SessionConnectNode) {
        controllerFor(node)!!.removeSession(node)
    }

    @Nonnull
    @CheckReturnValue
    private fun controllerFor(@Nonnull node: SessionController.SessionConnectNode): SessionController? {
        return shardControllers[node.shardInfo.shardId % shardControllers.size]
    }

    init {
       require(bucketFactor >= 1) { "Bucket factor must be at least 1" }
        shardControllers = arrayOfNulls(bucketFactor)
        for (i in 0 until bucketFactor) {
            shardControllers[i] = PrioritizingSessionController(homeGuildId)
        }
    }
}