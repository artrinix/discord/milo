package xyz.artrinix.milo.common.database.types

enum class MessageType {
    MESSAGE, EMBED, IMAGE
}