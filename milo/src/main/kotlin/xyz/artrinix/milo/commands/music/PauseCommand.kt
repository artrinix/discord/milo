package xyz.artrinix.milo.commands.music

import dev.schlaubi.lavakord.audio.Link
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo

class PauseCommand : AviationCog {
    @Command
    suspend fun pause(ctx: AviationContext) {
        val link = Milo.lavalink.getLink(ctx.guild.id)
        if (link.state == Link.State.NOT_CONNECTED) {
            ctx.send("I'm not playing anything hence no need to pause")
            return
        }
        link.player.pause(!link.player.paused)
        if (link.player.paused) {
            ctx.send("Playback has been resumed!")
        } else {
            ctx.send("Playback has been paused!")
        }
    }
}