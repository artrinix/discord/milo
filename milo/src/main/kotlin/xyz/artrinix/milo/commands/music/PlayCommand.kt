package xyz.artrinix.milo.commands.music

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.await
import dev.schlaubi.lavakord.audio.Link
import dev.schlaubi.lavakord.rest.TrackResponse
import dev.schlaubi.lavakord.rest.loadItem
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.annotations.Greedy
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo
import java.awt.Color

class PlayCommand : AviationCog {
    @Command
    suspend fun play(ctx: AviationContext, @Greedy query: String) {
        val vc = ctx.member!!.voiceState?.channel ?: return ctx.send("Please join a channel").let { }
        val link = Milo.lavalink.getLink(ctx.guild.id)
        if (link.state == Link.State.NOT_CONNECTED) link.connectAudio(vc.idLong.toULong())

        val search = if (query.startsWith("http")) {
            query
        } else {
            "ytsearch:$query"
        }

        val player = link.player

        val item = link.loadItem(search)

        when (item.loadType) {
            TrackResponse.LoadType.TRACK_LOADED -> {
                val track = item.tracks.first()
                link.player.playTrack(track)

                ctx.channel.sendMessageEmbeds(Embed {
                    author { name = "Now playing" }
                    title = track.info.title
                    url = track.info.uri
                    field { title = "Uploaded by"; value = track.info.author }
                    footer { title = "Requested by ${ctx.author.asTag}"; iconUrl = ctx.author.avatarUrl }
                }).await()
            }
            TrackResponse.LoadType.PLAYLIST_LOADED -> {
                val track = item.tracks.first()
                link.player.playTrack(track)

                ctx.channel.sendMessageEmbeds(Embed {
                    author { name = "Now playing from playlist" }
                    title = track.info.title
                    url = track.info.uri
                    thumbnail = "https://img.youtube.com/vi/${track.info.identifier}/hqdefault.jpg"
                    field { title = "Uploaded by"; value = track.info.author }
                    footer { title = "Requested by ${ctx.author.asTag}"; iconUrl = ctx.author.avatarUrl }
                }).await()
            }
            TrackResponse.LoadType.SEARCH_RESULT -> {
                val track = item.tracks.first()
                link.player.playTrack(track)

                ctx.channel.sendMessageEmbeds(Embed {
                    author { name = "Now playing from search request" }
                    title = track.info.title
                    url = track.info.uri
                    field { title = "Uploaded by"; value = track.info.author }
                    footer { title = "Requested by ${ctx.author.asTag}"; iconUrl = ctx.author.avatarUrl }
                }).await()
            }
            TrackResponse.LoadType.NO_MATCHES -> {
                ctx.channel.sendMessageEmbeds(Embed {
                    color = Color.RED.rgb
                    title = "Unable to find anything for the query"
                    description = "> $query"
                    footer { title = "Searched by ${ctx.author.asTag}"; iconUrl = ctx.author.avatarUrl }
                }).await()
            }
            TrackResponse.LoadType.LOAD_FAILED -> {
                ctx.channel.sendMessageEmbeds(Embed {
                    color = Color.RED.rgb
                    title = "Sorry, I failed to play that"
                    description = item.exception?.message ?: "Unknown error"
                }).await()
            }
        }
    }
}