package xyz.artrinix.milo.commands.music

import dev.schlaubi.lavakord.audio.Link
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo
import kotlin.time.DurationUnit

class SeekCommand : AviationCog {
    @Command
    suspend fun seek(ctx: AviationContext, time: Int) {
        val link = Milo.lavalink.getLink(ctx.guild.id)
        if (link.state == Link.State.NOT_CONNECTED) {
            ctx.send("Unable to seek nothing.")
            return
        }
        val player = link.player
        val input = time.toLong() * 1000

        val track = player.playingTrack
        if (track == null) {
            ctx.send("Not playing anything")
            return
        }

        val newPosition = player.position + input

        if (newPosition < 0 || newPosition > track.length.toLong(DurationUnit.MILLISECONDS)) {
            ctx.send("Cannot seek to before or after the song.")
            return
        }

        player.seekTo(newPosition)
    }
}