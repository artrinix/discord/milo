package xyz.artrinix.milo.commands.utility

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.await
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext

class PingCommand : AviationCog {
    @Command(description = "Ping, Pong")
    suspend fun ping(ctx: AviationContext) {
        ctx.message.channel.sendMessageEmbeds(Embed {
            author { name = "I made a request for you." }
            color = 0xFFFFFF
            field { name = ctx.translate("ping:gateway"); value = ctx.jda.gatewayPing.toString() }
            field { name = ctx.translate("ping:rest"); value = ctx.jda.restPing.await().toString() }
        }).reference(ctx.message).await()
    }
}