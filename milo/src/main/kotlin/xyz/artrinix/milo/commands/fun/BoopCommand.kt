package xyz.artrinix.milo.commands.`fun`

import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext

class BoopCommand : AviationCog {
    @Command()
    suspend fun boop(ctx: AviationContext) {
        ctx.reply(">/////< Noooo don't boop me")
    }
}