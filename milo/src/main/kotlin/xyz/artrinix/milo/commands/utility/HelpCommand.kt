package xyz.artrinix.milo.commands.utility

import dev.minn.jda.ktx.Embed
import net.dv8tion.jda.api.Permission
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo
import xyz.artrinix.milo.common.bot.MiloFramework

class HelpCommand : AviationCog {
    @Command()
    suspend fun help(ctx: AviationContext, category: String?) {
        val commands = Milo.aviation.commands.entries
            // Filter out hidden commands and stuff only for developers (eval and system commands)
            .filter { (_, v) -> !v.properties.hidden || !(v.properties.developerOnly && Milo.aviation.ownerIds.contains(ctx.author.idLong)) }
            // Don't show NSFW commands in non-nsfw channels, not that we plan to add any
            .filter { (_, v) -> if (!ctx.message.textChannel.isNSFW) !v.properties.nsfw else true }
            .groupBy { it.value.category }.toMutableMap()

        // Filter moderation commands to their respective permissions
        if (!Milo.aviation.ownerIds.contains(ctx.author.idLong)) commands.remove("Developer")
        if (ctx.member?.hasPermission(Permission.ADMINISTRATOR) == false) commands.remove("Admin")
        if (ctx.member?.hasPermission(Permission.MESSAGE_MANAGE) == false) commands.remove("Moderation")

        if (category.isNullOrEmpty()) {
            ctx.send(Embed {
                title = "Hi, I'm Milo, these are my command categories"
                description = commands.map { "> ${it.key}" }.joinToString("\n")
            })
        } else {
            val commandCategory = commands[category]
            if (commandCategory == null) {
                ctx.send("That category doesn't exist.")
                return
            } else {
                ctx.send(Embed {
                    title = category
                    description = commandCategory.joinToString("\n") { (k, v) -> "`$k` :: ${v.properties.description}" }
                })
            }
        }
    }
}