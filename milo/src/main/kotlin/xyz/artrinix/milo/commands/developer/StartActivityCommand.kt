package xyz.artrinix.milo.commands.developer

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.utils.config.ConfigFramework

class StartActivityCommand : AviationCog {
    private val client = HttpClient(CIO) {
        install(JsonFeature) {
            serializer = KotlinxSerializer(kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
            })
        }
    }

    @Serializable
    private data class ActivityInviteRequest(
        val target_type: Int,
        val target_application_id: Long,
        val max_age: Int = 86400,
        val max_uses: Int = 0,
        val temporary: Boolean = false
    )

    @Serializable
    private data class ActivityInviteResponse(
        val code: String
    )

    @Command
    suspend fun startActivity(ctx: AviationContext, activity: String? = "youtube") {
        val activityId = when (activity) {
            "youtube" -> 880218394199220334
            "youtubedev" -> 880218832743055411
            "poker" -> 755827207812677713
            "betrayal" -> 773336526917861400
            "fishing" -> 814288819477020702
            "chess" -> 832012774040141894
            "chessdev" -> 832012586023256104
            else -> 880218394199220334
        }

        val voiceState = ctx.member!!.voiceState!!
        if (!voiceState.inAudioChannel()) {
            ctx.send("Please join a voice channel to use this!")
            return
        }
        val channel = voiceState.channel!!
        val response: HttpResponse = client.post("https://discord.com/api/v9/channels/${channel.id}/invites") {
            contentType(ContentType.Application.Json)
            headers {
                append("Authorization", "Bot ${ConfigFramework.config.milo.token}")
            }
            body = ActivityInviteRequest(2, activityId)
        }

        val code = Json { ignoreUnknownKeys = true }.decodeFromString<ActivityInviteResponse>(response.readText()).code

        ctx.reply("https://discord.gg/$code")
    }
}