package xyz.artrinix.milo.commands.developer

import dev.minn.jda.ktx.await
import io.lettuce.core.internal.Futures.await
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.MessageEmbed
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.annotations.Greedy
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo
import xyz.artrinix.milo.common.bot.MiloFramework
import java.util.concurrent.CompletableFuture
import javax.script.ScriptEngineManager
import javax.script.ScriptException

class EvalCommand : AviationCog {
    private val engine = ScriptEngineManager().getEngineByExtension("kts")

    @Command(description = "Evaluate Kotlin code.", developerOnly = true)
    suspend fun eval(ctx: AviationContext, @Greedy code: String) {
        val bindings = mutableMapOf(
            "ctx" to ctx,
            "core" to Milo,
            "milo" to MiloFramework
        )

        if (engine == null) {
            ctx.reply("Kotlin Script Engine failed to start!")
            return
        }

        val stripped = code.replace("^```\\w+".toRegex(), "").removeSuffix("```")

        try {
            val result = engine.eval(stripped, engine.createBindings().apply { bindings.forEach(::put) })
                ?: return ctx.message.addReaction("👌").queue()

            if (result is CompletableFuture<*>) {
                val m = sendMessage(ctx, "```\nCompletableFuture<Pending>```")
                result.whenComplete { r, exception ->
                    val post = exception ?: r
                    m.editMessage("```kotlin\n$post\n```").queue()
                }
            } else {
                sendMessage(ctx, "```kotlin\n${result.toString().take(1950)}\n```")
            }
        } catch (e: ScriptException) {
            sendMessage(ctx, "Invalid script provided!\n```kotlin\n${e.localizedMessage}\n```")
        } catch (e: Exception) {
            sendMessage(ctx, "An exception occurred.\n```kotlin\n${e.localizedMessage}\n```")
        }
    }

    private suspend fun sendMessage(ctx: AviationContext, content: String): Message {
        return if (ctx.message.isEdited) {
            val history = ctx.channel.history.retrievePast(3).await()
            val message = history.find { it.messageReference?.message?.author?.idLong == ctx.author.idLong }
            if (message == null) ctx.channel.sendMessage(content).reference(ctx.message).await()
            else message.editMessage(content).await()
        } else {
            ctx.channel.sendMessage(content).reference(ctx.message).await()
        }
    }
}