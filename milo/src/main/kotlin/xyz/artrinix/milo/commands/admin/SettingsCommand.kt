package xyz.artrinix.milo.commands.admin

import dev.minn.jda.ktx.await
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Role
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.annotations.SubCommand
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.common.database.tables.guild.Guild
import xyz.artrinix.milo.common.database.tables.guild.GuildTable
import xyz.artrinix.milo.common.database.tables.guild.Welcomer
import xyz.artrinix.milo.common.database.tables.guild.autoRole.AutoRole
import xyz.artrinix.milo.common.database.tables.guild.leveling.ExperienceRole
import xyz.artrinix.milo.common.database.tables.guild.leveling.ExperienceRoleTable
import xyz.artrinix.milo.common.database.tables.guild.leveling.Leveling
import xyz.artrinix.milo.common.database.tables.guild.leveling.LevelingAnnouncement
import xyz.artrinix.milo.utils.newSuspendedTransaction
import java.util.*

class SettingsCommand : AviationCog {
    @Command(developerOnly = true)
    suspend fun settings(ctx: AviationContext) {
        val guildConfig = transaction {
            Guild.find { GuildTable.id eq ctx.guild.idLong }.firstOrNull() ?: run {
                val autoRoleD = transaction { AutoRole.new(ctx.guild.idLong) {} }

                val levelingAnnouncementD = transaction {
                    LevelingAnnouncement.new(ctx.guild.idLong) {
                        message = "Congratulations {{nickname}}, you're now level {{level}}!"
                    }
                }

                val levelingD = transaction {
                    Leveling.new(ctx.guild.idLong) {
                        announcement = levelingAnnouncementD
                    }
                }

                val welcomerD = transaction { Welcomer.new(ctx.guild.idLong) {} }

                Guild.new(ctx.guild.idLong) {
                    autoRole = autoRoleD
                    leveling = levelingD
                    welcomer = welcomerD
                }
            }
        }

        ctx.channel.sendMessage(guildConfig.readValues.toString()).await()
    }

    @SubCommand
    suspend fun autoRole(ctx: AviationContext) {
        val autoRole = transaction {
            AutoRole.findById(ctx.guild.idLong)
        }
        if (autoRole == null) {
            ctx.channel.sendMessage("No config found").await()
            return
        }

        ctx.channel.sendMessage(autoRole.readValues.toString()).await()
    }

    @SubCommand
    suspend fun setAutoRole(ctx: AviationContext, role: Role) {
        val autoRole = transaction {
            AutoRole.findById(ctx.guild.idLong)
        }
        if (autoRole == null) {
            ctx.channel.sendMessage("No config found").await()
            return
        }

        transaction {
            autoRole.enabled = true
            autoRole.roles = arrayOf(role.idLong, *autoRole.roles)
            autoRole.giveRolesAfter = 0
        }

        val nAutoRole = transaction { AutoRole.findById(ctx.guild.idLong) }!!
        ctx.channel.sendMessage(nAutoRole.readValues.toString()).await()
    }

    @SubCommand
    suspend fun enableLeveling(ctx: AviationContext, isEnabled: Boolean) {
        val announcementConfig = newSuspendedTransaction { LevelingAnnouncement.findById(ctx.guild.idLong) ?: LevelingAnnouncement.new { } }
        val levelConfig = newSuspendedTransaction { Leveling.findById(ctx.guild.idLong) ?: Leveling.new(ctx.guild.idLong) {
            announcement = announcementConfig
        } }

        newSuspendedTransaction {
            levelConfig.enabled = isEnabled
        }

        ctx.reply("Set \"enabled\" to \"$isEnabled\" for leveling.")
    }

    @SubCommand
    suspend fun addRoleToLeveling(ctx: AviationContext, level: Long, role: Role) {
        val levelConfig = newSuspendedTransaction { Leveling.findById(ctx.guild.idLong) ?: Leveling.new(ctx.guild.idLong) {} }

        val experienceRole = newSuspendedTransaction {
            ExperienceRole.find { ExperienceRoleTable.guildId eq ctx.guild.idLong and (ExperienceRoleTable.requiredLevel eq level) }
            ExperienceRole.new {
                guildId = ctx.guild.idLong
                roles = roles.toMutableList().apply { add(role.idLong) }.toTypedArray()
                requiredLevel = level
            }
        }

        newSuspendedTransaction {
            levelConfig.roles = SizedCollection(levelConfig.roles.toMutableList().apply { add(experienceRole) })
        }

        ctx.channel.sendMessage("Added ${role.asMention} to level $level's rewards.").reference(ctx.message).allowedMentions(listOf()).await()
    }
}