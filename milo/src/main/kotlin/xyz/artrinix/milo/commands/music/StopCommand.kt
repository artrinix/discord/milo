package xyz.artrinix.milo.commands.music

import dev.schlaubi.lavakord.audio.Link
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.Milo

class StopCommand : AviationCog {
    @Command
    suspend fun stop(ctx: AviationContext) {
        val link = Milo.lavalink.getLink(ctx.guild.id)
        if (link.state == Link.State.NOT_CONNECTED) {
            ctx.send("There is nothing to stop")
            return
        }

        link.player.stopTrack()
        link.destroy()
        ctx.send("Playback stopped, I have left to save resources.")
    }
}