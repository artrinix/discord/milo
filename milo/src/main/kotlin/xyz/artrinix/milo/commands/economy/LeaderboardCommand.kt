package xyz.artrinix.milo.commands.economy

import dev.minn.jda.ktx.Embed
import dev.minn.jda.ktx.await
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.common.database.tables.guild.GuildUser
import xyz.artrinix.milo.common.modules.leveling.LevelingModule
import xyz.artrinix.milo.utils.newSuspendedTransaction
import java.awt.Color
import kotlin.properties.Delegates

class LeaderboardCommand : AviationCog {
    @Command(aliases = ["lb", "ranktop", "top"])
    suspend fun leaderboard(ctx: AviationContext) {
        val profile = newSuspendedTransaction { GuildUser.findById(ctx.author.idLong) }
        val leaderboard = LevelingModule.leaderboard()
        val mappedLeaderboard = leaderboard
            .map {
                val user = ctx.jda.getUserById(it.id.value)
                "> **${leaderboard.indexOf(it) + 1}** | Level **${it.level}** - **${it.xp}**/**${LevelingModule.requiredXpForLevel(it.level + 1)}** | **${user?.asTag ?: "Unknown User" }**"
            }

        ctx.channel.sendMessageEmbeds(Embed {
            title = "Milo Leveling Leaderboard"
            description = mappedLeaderboard.take(10).joinToString("\n")
            color = Color.decode("#F2CDCD").rgb
        }).reference(ctx.message).await()
    }
}