package xyz.artrinix.milo.commands.developer

import net.dv8tion.jda.api.JDA
import xyz.artrinix.aviation.command.annotations.Command
import xyz.artrinix.aviation.command.entities.AviationCog
import xyz.artrinix.aviation.command.entities.AviationContext
import xyz.artrinix.milo.utils.splitToList

class ShardInfoCommand : AviationCog {
    @Command(aliases = ["shards"], developerOnly = true)
    suspend fun shardInfo(ctx: AviationContext) {
        val stats: MutableMap<Int, JDA> = mutableMapOf()

        ctx.jda.shardManager!!.shards.forEach { stats[it.shardInfo.shardId] = it }

        val status = stats.entries.sortedBy { it.key }
            .joinToString("\n") { formatInfo(it.key, stats.size, it.value) }

        val pages = status.splitToList(1920)

        for (page in pages) {
            ctx.send("```prolog\n" +
                    " ID |    STATUS |    PING | GUILDS |  USERS |  VC\n" +
                    "$page```")
        }
    }

    private fun formatInfo(id: Int, total: Int, jda: JDA): String {
        return "%3d | %9.9s | %7.7s | %6d | %6d | %3d".format(
            id,
            jda.shardManager!!.statuses[jda]!!.name,
            "${jda.gatewayPing}ms",
            jda.guildCache.size(),
            jda.userCache.size(),
            0
        )
    }

    private fun getShardIdForGuild(guildId: Long, shardCount: Int): Int = ((guildId shr 22) % shardCount).toInt()
}