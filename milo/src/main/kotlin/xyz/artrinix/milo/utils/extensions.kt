package xyz.artrinix.milo.utils

import kotlinx.coroutines.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction
import xyz.artrinix.milo.Milo
import java.sql.Connection
import java.util.concurrent.Executors
import java.util.concurrent.ThreadFactory
import kotlin.math.pow

fun createThreadPool(name: String) = Executors.newCachedThreadPool { Thread(null, null, name, 0) }
val CoroutineExecutor = createThreadPool("Coroutine Executor Thread %d")
val CoroutineDispatcher = CoroutineExecutor.asCoroutineDispatcher()

fun String.splitToList(limit: Int = 2000): List<String> {
    val pages = mutableListOf<String>()

    val lines = this.split("\n").dropLastWhile { it.isEmpty() }
    var chunk = StringBuilder()

    for (line in lines) {
        if (chunk.isNotEmpty() && chunk.length + line.length > limit) {
            pages.add(chunk.toString())
            chunk = StringBuilder()
        }

        if (line.length > limit) {
            val lineChunks = line.length / limit

            for (i in 0 until lineChunks) {
                val start = limit * i
                val end = start + limit
                pages.add(line.substring(start, end))
            }
        } else {
            chunk.append(line).append("\n")
        }
    }

    if (chunk.isNotEmpty()) {
        pages.add(chunk.toString())
    }

    return pages.toList()
}

fun Array<*>.intersects(other: Array<*>) = intersect(other.toSet()).isNotEmpty()

suspend fun <T> newSuspendedTransaction(repetitions: Int = 5, database: Database = Milo.database.db, statement: Transaction.() -> T): T = withContext(
    Dispatchers.IO) {

    transaction(Connection.TRANSACTION_SERIALIZABLE, repetitions, database) {
        statement.invoke(this)
    }
}

@OptIn(DelicateCoroutinesApi::class)
suspend fun <T> suspendedTransactionAsync(statement: Transaction.() -> T) = GlobalScope.async(CoroutineDispatcher) {
    newSuspendedTransaction(statement = statement)
}