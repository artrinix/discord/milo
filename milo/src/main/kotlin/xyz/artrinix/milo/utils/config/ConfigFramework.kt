package xyz.artrinix.milo.utils.config

import com.charleskorn.kaml.Yaml
import dev.minn.jda.ktx.SLF4J
import java.io.File
import kotlin.system.exitProcess

class ConfigFramework(private val path: String = "config.yaml") {
    private val log by SLF4J

    fun loadConfig() {
        try {
            val file = File(path)
            if (!file.exists()) {
                file.createNewFile()
                Thread.sleep(100)
                file.writeText(Yaml.default.encodeToString(ConfigData.serializer(), ConfigData()))
                log.info("Generated default config, please fill it out before continuing.")
                exitProcess(1)
            } else {
                config = Yaml.default.decodeFromString(ConfigData.serializer(), file.readText())

                config.milo.sharding.start = config.milo.sharding.start.takeIf { it >= 0 }
                    ?: error("sharding.start needs to be >= 0")
                config.milo.sharding.end = config.milo.sharding.end.takeIf { it in config.milo.sharding.start..config.milo.sharding.total || it == -1 }
                    ?: error("sharding.end needs to be <= sharding.total")
            }
        } catch(error: Exception) {
            log.error("Failed to load config, unfortunately we cannot continue without these values.", error)
            exitProcess(1)
        }
    }

    companion object {
        var config: ConfigData = ConfigData()
            private set
    }
}